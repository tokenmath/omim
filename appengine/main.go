package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var caches = make(map[interface{}]CacheMap)

var CacheTimeout = (2 * time.Hour).Nanoseconds()

var status = &StatusCache{Status: make(map[interface{}]string)}

var StatusTimeout = (5 * time.Minute).Nanoseconds()

// cache

type Cache struct {
	Last int64
	Url  string
}

type CacheMap map[string]*Cache

type StatusCache struct {
	Last      int64
	Dev       string
	DevMd5    string
	DevErr    error
	Master    string
	MasterMd5 string
	MasterErr error
	Status    map[interface{}]string
}

// tokens

type DirectToken string

type DropboxToken string

type GoogleDriveToken struct {
	FolderID string
	Token    string
}

// commons

func DirectLink(token DirectToken, path string) (string, error) {
	return string(token) + path, nil
}

func md5str(s string) string {
	x := md5.Sum([]byte(s))
	return hex.EncodeToString(x[:])
}

func domain(s string) (string, error) {
	u, err := url.Parse(s)
	if err != nil {
		return "", err
	}
	parts := strings.Split(u.Hostname(), ".")
	l := len(parts)
	if l > 2 {
		parts = parts[l-2:]
	}
	return strings.Join(parts, "."), nil
}

func downloaduri(client *http.Client, uri string) (string, error) {
	req, _ := http.NewRequest("GET", uri, nil)
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	if resp.StatusCode != 200 {
		return "", fmt.Errorf(http.StatusText(resp.StatusCode))
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	return buf.String(), nil
}

func jq(data string, path string) (string, error) {
	var objmap map[string]*json.RawMessage
	err := json.Unmarshal([]byte(data), &objmap)
	var s interface{}
	err = json.Unmarshal(*objmap[path], &s)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%v", s), nil
}

func jquri(client *http.Client, uri string, path string) (string, string, error) {
	s, err := downloaduri(client, uri)
	if err != nil {
		return "", "", err
	}
	m := md5str(s)
	s, err = jq(s, path)
	if err != nil {
		return "", "", err
	}
	return s, m, nil
}

func downloadlink(client *http.Client, token interface{}, path string) (string, error) {
	now := time.Now().UnixNano()

	var link string
	var err error
	var cache CacheMap

	if c, ok := caches[token]; ok {
		cache = c
	} else {
		cache = make(CacheMap)
		caches[token] = cache
	}

	if val, ok := cache[path]; ok && val.Last+CacheTimeout > now {
		link = val.Url
	} else {
		switch token.(type) {
		case DropboxToken:
			link, err = GetTemporaryLink(client, token.(DropboxToken), path)
		case GoogleDriveToken:
			link, err = GoogleDriveLink(client, token.(GoogleDriveToken), path)
		case DirectToken:
			link, err = DirectLink(token.(DirectToken), path)
		}
		if err != nil {
			return "", err
		}
		cache[path] = &Cache{now, link}
	}
	return link, nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)

	now := time.Now().UnixNano()

	switch path {
	case "/favicon.ico", "/robots.txt":
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "nope!")
		return
	case "/":
		fmt.Fprintf(w, "<html><pre>")
		fmt.Fprintf(w, "omim status:\n\n")
		var last string // last map version dev prior to master
		if status.Last+StatusTimeout < now {
			status.Dev, status.DevMd5, status.DevErr = jquri(client, "https://gitlab.com/axet/omim/raw/dev/data/countries.txt?inline=false", "v")
			status.Master, status.MasterMd5, status.MasterErr = jquri(client, "https://gitlab.com/axet/omim/raw/master/data/countries.txt?inline=false", "v")
		}
		if status.DevErr != nil {
			fmt.Fprintf(w, "  - dev: down %v!", status.DevErr)
		} else {
			fmt.Fprintf(w, "  - dev: %s (%s)\n", status.Dev, status.DevMd5)
		}
		if status.MasterErr != nil {
			fmt.Fprintf(w, "  - master: down %v!", status.MasterErr)
		} else {
			fmt.Fprintf(w, "  - master: %s (%s)\n", status.Master, status.MasterMd5)
		}
		fmt.Fprintf(w, "\n")
		if status.Dev == status.Master {
			last = status.Master
			fmt.Fprintf(w, "dev == master: no updates are pending\n")
		} else {
			last = status.Dev
			fmt.Fprintf(w, "dev != master: updates are pending, please prepare your mirror!\n")
		}
		fmt.Fprintf(w, "\n")

		cm := ""
		fmt.Fprintf(w, "mirrors are: \n\n")
		for _, t := range tokens {
			var err error

			if status.Last+StatusTimeout < now {
				var c, cd, cs string
				c, err = downloadlink(client, t, "/"+last+"/countries.txt")
				if err == nil {
					cd, err = downloaduri(client, c)
					if err == nil {
						cs = md5str(cd)
					}
				}

				if cm == "" {
					cm = cs
				}

				if cs == "" {
					status.Status[t] = fmt.Sprintf("%v", err)
				} else if cm != cs {
					status.Status[t] = fmt.Sprintf("countries.txt are differ (%s)!!!", cs)
				} else {
					status.Status[t] = fmt.Sprintf("ok (%s)", cs)
				}
			}

			switch t.(type) {
			case DropboxToken:
				fmt.Fprintf(w, "  - Dropbox: %s - %s", md5str(string(t.(DropboxToken))), status.Status[t]) // token has rw access, show md5 instead
			case GoogleDriveToken:
				fmt.Fprintf(w, "  - GoogleDrive: %s - %s", string(t.(GoogleDriveToken).FolderID), status.Status[t])
			case DirectToken:
				d, _ := domain(string(t.(DirectToken)))
				fmt.Fprintf(w, "  - Direct: %s - %s", d, status.Status[t])
			}
			fmt.Fprintf(w, "\n")
		}

		if status.Last+StatusTimeout < now {
			status.Last = now
			log.Printf("Updating StatusCache %d\n", status.Last)
		}
		fmt.Fprintf(w, "</pre></html>")
		return
	}

	t := rand.Intn(len(tokens))
	token := tokens[t]

	link, err := downloadlink(client, token, path)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "error: %v!", err)
		return
	}

	http.Redirect(w, r, link, 301)
}

func init() {
	http.HandleFunc("/_ah/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ok")
	})

	http.HandleFunc("/", handler)
}
