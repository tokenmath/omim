#!/bin/bash
#

set -e

# define those vaiables in bash before startin:
#
# export DROPBOX=dropboxtoken
# export MAP=190113

DATA="$PWD/../data"
URL="${URL:-http://opensource-data.mapswithme.com/regular/weekly/}"
MAP="${MAP:-$(cat $DATA/countries.txt | jq .v)}"

import() {
	local URL="$1"
	local NAME="${URL##*/}"
	local FNAME=${NAME%.*}
	eval "${FNAME}() { ( [ -e \"./$NAME\" ] && \"./$NAME\" \"\$@\" ) ||
	  ( which \"$NAME\" > /dev/null && \"$NAME\" \"\$@\" ) ||
	  ( wget -O \"./$NAME\" \"$URL\" && chmod +x \"./$NAME\" && \"./$NAME\" \"\$@\" ) }"
}

import "https://gitlab.com/axet/homebin/raw/mac/apache2json.sh"

while read -r N S; do
	[[ $N =~ ttf|obsolete ]] && FONTS+=( "$N" ) || WORLD+=( "$N" )
done < "$DATA/external_resources.txt"

# data/borders/*.poly -> *.mwm
# IFS=$'\n' read -d '' -r -a MAPS < <(for D in $DATA/borders/*.poly; do D=${D##*/}; echo ${D%.*}.mwm; done) || true

#LIST=( "${WORLD[@]}" "${MAPS[@]}" )

IFS=$'\n' read -d '' -r -a LIST < <(apache2json -R '\.\.\\*' "$URL" | jq -r '.[] .name') || true

dropbox_upload() {
	local P="$1"
	local U=$(curl --silent -X POST "https://api.dropboxapi.com/2/files/get_temporary_upload_link" \
	    --header "Authorization: Bearer $DROPBOX" \
	    --header "Content-Type: application/json" \
	    --data "{\"commit_info\": {\"path\": \"$P\",\"mode\": \"overwrite\",\"autorename\": false,\"mute\": false,\"strict_conflict\": false},\"duration\": 3600}" | jq -r '.link')
	curl --silent -X POST "$U" \
		--header "Content-Type: application/octet-stream" \
		--data-binary @- > /dev/null
}

googledrive_upload() {
	local P="$1"
	local FOLDERID="[folderid]"
	local ID=$(curl --silent -X POST "https://www.googleapis.com/drive/v3/files?key=$GOOGLE" \
	    --header 'Accept: application/json' \
	    --header 'Content-Type: application/json' \
	    --data "{\"name\":\"$P\",\"parents\":[\"$FOLDERID\"]}")
	curl --silent -X PATCH "https://www.googleapis.com/upload/drive/v3/files/$ID?key=$GOOGLE" \
		--header "Content-Type: application/octet-stream" \
		--data-binary @- > /dev/null
}

process() {
	local M="$1"
	if [ -n "$DROPBOX" ]; then
		dropbox_upload "/$MAP/$M"
	elif [ -n "$GOOGLE" ]; then
		googledrive_upload "/$MAP/$M"
	else
		mkdir -p "$MAP" && cat > "$MAP/$M"
	fi
}

for I in "${!LIST[@]}"; do
	M="${LIST[I]}"
	echo "$M"
	wget -qO - "$URL/$M" | process "$M"
done

for I in "${!FONTS[@]}"; do
	M="${FONTS[I]}"
	echo "$M"
	cat "$DATA/$M" | process "$M"
done
