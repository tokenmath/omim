#!/bin/bash

set -e

urldecode() {
    local U="${1//+/ }"
    printf '%b\n' "${U//%/\x}"
}

matches() {
  local e u="$1"
  shift
  for e; do [[ $u =~ $e ]] && return 0; done
  return 1
}

extracturis() {
	local EXCLUDE=()

	while [[ $# > 0 ]]; do
	key="$1"
	case $key in
	    -R|--reject)
	    EXCLUDE=( "${EXCLUDE[@]}" "$2")
	    shift # past argument
	    ;;
	    *)
	    break
	    ;;
	esac
	shift # past argument or value
	done

	if [ $# -eq 0 ]; then
	  return 1
	fi

	local URL="$1"
	shift

	wget -qO - "$URL" | xmlstarlet fo -H -R | xmlstarlet sel -t -v '//a/@href' -n | while read -r U; do matches "${U}" "${EXCLUDE[@]}" && continue; urldecode "$U"; done
}

extracturis "$@"
